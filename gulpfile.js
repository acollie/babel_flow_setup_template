const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const rename = require("gulp-rename");
const babel = require("gulp-babel");
const watch = require("gulp-watch");
const browserify = require("browserify");
const browserSync = require("browser-sync");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");
const vinylPaths = require("vinyl-paths");
const uglify = require("gulp-uglify");
const del = require("del");

const jsMain = "main.js";
const jsSrcFolder = "src/";
const jsDest = "./dist/";
const jsFiles = [jsMain];
const htmlSrcFiles = "./src/**/*.html";
const jsSrcFiles = "./src/**/*.js";


gulp.task("browserSync", () => {
    browserSync({
        server: {
            baseDir: jsDest
        }
    });
});

gulp.task("clean", () => {
    return gulp.src( jsDest, { read: false } )
    .pipe(vinylPaths(del));
});

function copy() {
    return gulp.src( htmlSrcFiles )
    .pipe( gulp.dest( jsDest ) )
    .pipe( browserSync.reload( {stream: true} ) );

}

gulp.task("copy", copy);


function compile(cb) {
    jsFiles.map(function( entry ){
        return browserify({
            basedir: ".",
            debug: true,
            entries: [jsSrcFolder + jsMain],
            cache: {},
            packageCache: {}
        })
        .transform( "babelify" )
        .bundle()
        .pipe( source( entry ) )
        .pipe( rename({ extname: ".min.js" }) )
        .pipe( buffer() )
        .pipe(  sourcemaps.init( { loadMaps: true } ) )
        .pipe( uglify() )
        .pipe( sourcemaps.write("./") )
        .pipe( gulp.dest( jsDest ) )
        .pipe( browserSync.reload( { stream: true } ) );
    });

    cb();
}

gulp.task( "compile", compile );

gulp.task( "watchSrc", () => {
    gulp.watch( htmlSrcFiles, copy );
    gulp.watch( jsSrcFiles, {ignore: "gulpfile.js"}, compile );
});

gulp.task( "default", gulp.series( "clean", "copy", "compile", "browserSync", "watchSrc" ) );
