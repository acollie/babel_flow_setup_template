// @flow
const newOne = (name: string): void => {
    const helloWorld = document.getElementById('helloWorld');
    helloWorld.innerHTML = 'Hello ' + name + '...!';
};

newOne('Linzel');

const fizzbuzz = (): void => {
    const fizzBuzzTitle = document.getElementById("fizzBuzzTitle");
    const fizzBuzz = document.getElementById('fizzbuzz');
    let fb = '';
    let lineCount = 0;
    let htmlBody = document.body;

    document.body.appendChild(fizzBuzzTitle);

    for (let i = 1; i <= 200; i++) {
        if (i % 15 === 0) {
            fb += 'FizzBuzz\t';
        } else if (i % 3 === 0) {
            fb += 'Fizz\t';
        } else if (i % 5 === 0) {
            fb += 'Buzz\t';
        } else {
            fb += i.toString() + '\t';
        }

        if (i % 10 === 0) {
            fb += "\n";
            lineCount++;

            fizzBuzz.appendChild(document.createTextNode(fb));
            htmlBody.appendChild(fizzBuzz);

            fb = "";
        }
    }

    console.log(fb);
};

fizzbuzz();

const helloWorld = (name: string): string => {
    return 'Hello ' + name + '...!\nWelcome to Javascript!!!';
}

const onLoad = (): void => {
    const welcomeTitle = document.getElementById("welcomeTitle");
    const welcome = document.getElementById('welcome');
    const greetings = helloWorld('Alex');

    welcome.appendChild(document.createTextNode(greetings));

    document.body.appendChild(welcomeTitle);
    document.body.appendChild(welcome);

}

onLoad();
